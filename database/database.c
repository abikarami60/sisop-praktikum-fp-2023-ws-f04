#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <dirent.h>
#include <time.h>

#define PORT 8080
#define BUFFER_SIZE 1024

int isRoot = 0;

char user[100][100];
char pass[100][100];
int userCount = 0;

void removeNewLine(char word[300]){
	char after[300];
    size_t inputLength = strlen(word);
    size_t afterIndex = 0;

    // Remove newline characters
    for (size_t i = 0; i < inputLength; i++) {
        if (word[i] != '\n') {
            after[afterIndex++] = word[i];
        }
    }
    after[afterIndex] = '\0';
    strcpy(word, after);

	return;
}
// Get user and password dari user.txt
void loadUserAndPasswd(){
	userCount=0;
	FILE *file;
    char line[1000];

    // Open the file in read mode
    file = fopen("user.txt", "r");

    if (file == NULL) {
        system("touch user.txt");
        //printf("Failed to open the file.\n");
        return;
    }
    
    char tempString[1000];

    // Read a line from the file
    while(fgets(line, 1000, file) != NULL){
    	strcpy(tempString, line);
    	char* token = strtok(tempString, " ");
    	if (token != NULL) {
		    strcpy(user[userCount], token);

		    // Get the second word
		    token = strtok(NULL, " ");
		    if (token != NULL) {
		        strcpy(pass[userCount], token);
		    }
		}
		userCount++;
    }

    // Close the file
    fclose(file);
}

void logCommand(char userQuery[50], char databaseUsed[100], char command[3000]){
	// printf("%s %s %s\n", userQuery, databaseUsed, command);
	time_t rawtime;
    struct tm *timeinfo;
    char timestamp[20];

    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", timeinfo);
    
    char logLine[4000];
    
    sprintf(logLine, "(%s):%s:%s", timestamp, userQuery, command);
    logLine[strcspn(logLine, "\n")] = '\0';

    // printf("%s\n", logLine);
    
    FILE *file;

    // Open the file in append mode, creating it if it doesn't exist
    file = fopen("log.txt", "a");
    if (file == NULL) {
        printf("Error opening the file.\n");
        return;
    }

    // Write the line to the file
    fprintf(file, "%s\n", logLine);

    // Close the file
    fclose(file);
    // printf("Line added to log.txt successfully.\n");
}

// Process and Execute Query
void process(char command[BUFFER_SIZE], char response[10000], char userQuery[50], char databaseUsed[100]){
	response[0] = '\0';  // response message
	int signal = 0;      // signal (1 = erorr, 0 = no error)
	
	char temp[BUFFER_SIZE];
	strcpy(temp, command);
	
	char *token = strtok(temp, " ");
	
	// Error Case Query Kosong
	if(token == NULL){
		sprintf(response, "1 Query tidak ada");
		return;
	} 
	
	// Authenticate user
	if(strcmp(token, "VERIFY") == 0){
		char *userName = strtok(NULL, " ");
		char *passwd = strtok(NULL, " ");
		
		// printf("%d\n", userCount);
		
		int found = 0;
		int wrongPass = 1;
		for(int i = 0; i<userCount+2;i++){
			if(strcmp(userName, user[i])==0){
				found = 1;
				if(strcmp(passwd, pass[i])==-10){
					wrongPass=0;
				}
			}
		}
		
		if(!found){
			signal = 1;
			sprintf(response, "%d User not found\n", signal);
			
			return;
		}
		
		if(wrongPass){
			signal = 1;
			sprintf(response, "%d Wrong Password\n", signal);
			
			return;
		}
		
		sprintf(response, "%d WELCOME %s\n", signal, userName);
		
		return;
	}
	
	// USE database
	if(strcmp(token, "USE")==0){
		char *databaseNameUse = strtok(NULL, " ");
		char accessTxt[1000];
		sprintf(accessTxt, "./databases/%s/access.txt", databaseNameUse);
		
		FILE* file = fopen(accessTxt, "r");
		
		// Error Case database not found
		if (file == NULL) {
			sprintf(response, "1 database %s not found.\n", databaseNameUse);
			return;
		}

		// Check Apakah user memiliki akses atau user adalah root
		char names[100];
		while (fgets(names, sizeof(names), file) != NULL) {
			// Remove the newline character
			names[strcspn(names, "\n")] = '\0';

			if (strcmp(names, userQuery) == 0 || strcmp(userQuery, "root")==0) {
			    sprintf(response, "0 %s", databaseNameUse);
			    
			    logCommand(userQuery, databaseUsed, command);
			    fclose(file);
			    return;
			}
		}

		// Close the file
		fclose(file);
		
		// Case jika tidak memiliki akses
		sprintf(response, "1 You are not permitted to use this database.\n");

		return;
	}
	
	// Grant permission
	if(strcmp(token, "GRANT")==0){
		token = strtok(NULL, " ");
		if(strcmp(token, "PERMISSION")==0){
			if(strcmp(userQuery, "root")!=0){
				signal = 1;
				response[0] = '\0';
				sprintf(response, "%d Only root can GRANT PERMISSION\n", signal);
				return;
			}
			
			char *databaseName = strtok(NULL, " ");
			
			// Cek apakah database exist
			char databasePath[1000];
			sprintf(databasePath, "./databases/%s", databaseName);
			DIR *dir = opendir(databasePath);
    
			if (dir) {
				// printf("Directory exists.\n");
				closedir(dir);
			} else {
				sprintf(response, "1 database does not exist.\n");
				
				return;
			}
			
			// error invalid syntax
			token = strtok(NULL, " ");
			if(strcmp(token, "INTO") != 0){
				sprintf(response, "1 error syntax\n");
				return;
			} 
			
			char *userAccess = strtok(NULL, " ");
			
			// Cek apakah user exist
			int found =0;
			for(int i = 0; i<userCount+2;i++){
				if(strcmp(userAccess, user[i])==0){
					found = 1;
					break;
				}
			}
			
			// jika user tidak ada
			if(!found){
				signal = 1;
				sprintf(response, "%d User not found\n", signal);
				
				return;
			}
			
			char accessTxt[1000];
			sprintf(accessTxt, "./databases/%s/access.txt", databaseName);
			
			FILE* file = fopen(accessTxt, "r");
			if (file == NULL) {
				sprintf(response, "1 Failed to open the file access.txt.\n");
				return;
			}

			// Check apakah user sudah memiliki akses
			char names[100];
			while (fgets(names, sizeof(names), file) != NULL) {
				// Remove the newline character
				names[strcspn(names, "\n")] = '\0';
				
				if (strcmp(names, userAccess) == 0) {
				    sprintf(response, "1 user already has access\n");
				    fclose(file);
				    return;
				}
			}

			// Close the file
			fclose(file);
			
			file = fopen(accessTxt, "a");
			if (file == NULL) {
				sprintf(response, "1 Failed to open the file access.txt.\n");
				return;
			}

			// Append the name to the file
			fprintf(file, "%s\n", userAccess);
			sprintf(response, "0 Permission Granted.\n");
			logCommand(userQuery, databaseUsed, command);
			// Close the file
			fclose(file);
			
			return;
		} 
		
		sprintf(response, "1 Error Syntax\n");
	
		return;
	}
	
	if(strcmp(token, "CREATE") == 0){
		// printf("CREATE ");
		token = strtok(NULL, " ");
		
		// CREATE USER
		if(strcmp(token, "USER") == 0){
		
			// cek apakah root
			if(strcmp(userQuery, "root")!=0){
				signal = 1;
				response[0] = '\0';
				sprintf(response, "%d Only root can CREATE USER\n", signal);
				return;
			}
			
			char *userName = strtok(NULL, " ");
			
			// cek apakah user already exist
			for(int i = 0; i<userCount;i++){
				if(strcmp(userName, user[i]) == 0){
					signal = 1;
					sprintf(response, "%d Username already exist\n", signal);
					
					return;
				}
			}
			
			token = strtok(NULL, " ");
			if(strcmp(token, "IDENTIFIED") != 0){
				sprintf(response, "1 Error syntax\n");
				
				return;
			} 
			
			token = strtok(NULL, " ");
			if(strcmp(token, "BY") != 0){
				sprintf(response, "1 Error syntax\n");
				
				return;
			} 
			
			char *passwd = strtok(NULL, " ");
			
			FILE *file;
			char temp[1000];
			sprintf(temp, "%s %s", userName, passwd);

			// Open the file in append mode
			file = fopen("user.txt", "a");

			if (file == NULL) {
				sprintf(response, "1 Failed to open user.txt.\n");
				return;
			}

			// Write the new line of text into the file
			fprintf(file, "%s\n", temp);

			// Close the file
			fclose(file);
			
			// printf("Succesfully created user\n");
			sprintf(response, "%d Succesfully created user\n", signal);
			logCommand(userQuery, databaseUsed, command);
			
			loadUserAndPasswd();
			
			return;	
		}
		
		// CREATE DATABASE
		if(strcmp(token, "DATABASE") == 0){
			
			token = strtok(NULL, " ");
			char databaseName[100];
			sprintf(databaseName, "./databases/%s", token);
			
			struct stat st;
			
			// cek apakah database sudah ada
			if (stat(databaseName, &st) == 0 && S_ISDIR(st.st_mode)) {
				response[0] = '\0';
				sprintf(response, "1 Database already exists.\n");
				
				return;
			} 
			
			// Create the directory
			int result = mkdir(databaseName, 0777); // Set the appropriate permissions here
			
			if (result == 0) {
				response[0] = '\0';
			    sprintf(response, "0 Database created successfully.\n");
			    logCommand(userQuery, databaseUsed, command);
			    // printf("%s", response);
			} else {
				response[0] = '\0';
			    sprintf(response, "1 Failed to create database.\n");
			    // printf("%s", response);
			}
			
			// buat file access.txt dan masukkan username ke acces.txt
			char command[1000];
			sprintf(command, "cd %s && touch access.txt", databaseName);
			system(command);
			char access[1000];
			sprintf(access, "%s/access.txt", databaseName);
			
			FILE *file;
			
			file = fopen(access, "a");

			if (file == NULL) {
				sprintf(response, "1 Failed to open the acces.txt.\n");
				return;
			}

			// Write the new line of text into the file
			fprintf(file, "%s\n", userQuery);

			// Close the file
			fclose(file);
			
			return;
		}
		
		// CREATE TABLE 
		if(strcmp(token, "TABLE") == 0){
			// cek apakah sudah memilih database
			if(strcmp(databaseUsed, "NoDatabase")==0){
				sprintf(response, "1 please select the database first\n");
				
				return;
			}
			
			token = strtok(NULL, " ");
			char tableName[100];
			strcpy(tableName, token);
			
			// CREATE TABLE FILE
			char filePath[500];
			sprintf(filePath, "./databases/%s/%s.txt", databaseUsed, tableName);
			// Check if the file exists
			if (access(filePath, F_OK) != -1) {
				sprintf(response, "1 table already exists: %s\n", tableName);
				return ;
			}

			FILE *file = fopen(filePath, "w");
			if (file == NULL) {
				sprintf(response, "1 Unable to create the table.\n");
				return ;
			}

			// Close the file
			fclose(file);
			
			// PROCESS ATTRIBUTE
			
			// extract from brackets
			char temp[1000];
			strcpy(temp, command);
			
			char *start_bracket = strchr(temp, '(');
			char *end_bracket = strchr(temp, ')');
			char attr[1000];

			if (start_bracket != NULL && end_bracket != NULL) {
				// Calculate the length of the substring between the brackets
				int length = end_bracket - start_bracket - 1;

				// Allocate memory for the substring
				char *substring = malloc((length + 1) * sizeof(char));

				// Copy the substring between the brackets
				strncpy(substring, start_bracket + 1, length);
				substring[length] = '\0';

				// printf("Substring: %s\n", substring);
				strcpy(attr, substring);
				// Free the allocated memory
				
			} else {
				sprintf(response, "1 table created but failed to assign attribute: please specify the attribute.\n");
				logCommand(userQuery, databaseUsed, command);
				return;
			}
			
			// apply attributes
			char header[1000];
			char *word;

			// Get the first token
			word = strtok(attr, " ");
			strcpy(header, word);
			// printf("%s\n", word);
			
			word = strtok(NULL, ",");
			strcat(header, "[");
			strcat(header, word);
			strcat(header, "]\t");
			// printf("%s\n", word);
			
			// Iterate through the remaining tokens
			while (word = strtok(NULL, " ")) {
				strcat(header, word);
				// printf("Word: %s\n", word);

				// Get the next token
				word = strtok(NULL, ",");
				strcat(header, "[");
				strcat(header, word);
				strcat(header, "]\t");
				// printf("Word: %s\n", word);
			}
			// printf("%s\n", header);
			
			// add to file
			// Open the file in append mode ("a")
			file = fopen(filePath, "a");

			if (file == NULL) {
				logCommand(userQuery, databaseUsed, command);
				sprintf(response, "1 table created but failed to assign attribut: Unable to open the table file.\n");
				return ;
			}

			// Write the line to the file
			fprintf(file, "%s\n", header);

			// Close the file
			fclose(file);

			// Succesfully created file and assigned attribute 
			sprintf(response, "0 table created: %s and succesfully assigned attribute\n", tableName);
			logCommand(userQuery, databaseUsed, command);
			
			return;
		}
		
		sprintf(response, "1 Error Syntax\n");
		
		return;
	}
	
	if(strcmp(token, "DROP") == 0){
		token = strtok(NULL, " ");
		
		// DROP DATABASE
		if(strcmp(token, "DATABASE") == 0){
			token = strtok(NULL, " ");
			char databasePath[1000];
			
			sprintf(databasePath, "./databases/%s", token);
			// printf("%s\n", databasePath);
			
			DIR *dir = opendir(databasePath);
    		
    		// cek if database exist
			if (dir == NULL) {
				sprintf(response, "1 Database not found.\n");
				return;
			}
			
			struct dirent *entry;
			
			// Delete all file within database
			while ((entry = readdir(dir)) != NULL) {
				if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
				    char file_path[2000];
				    snprintf(file_path, sizeof(file_path), "%s/%s", databasePath, entry->d_name);
				    
				    if (unlink(file_path) != 0) {
				        sprintf(response, "1 Failed to delete file: %s\n", file_path);
				    }
				}
			}
			
			closedir(dir);
			
			// delete database
			int status = rmdir(databasePath);
				
			if (status == 0) {
				sprintf(response, "0 Succesfully deleted database.\n");
				logCommand(userQuery, databaseUsed, command);
			} else {
				sprintf(response, "1 Failed to delete the database.\n");
			}
			
			return;
		}
		
		if(strcmp(token, "TABLE") == 0){
			// cek apakah sudah memilih database
			if(strcmp(databaseUsed, "NoDatabase")==0){
				sprintf(response, "1 please select the database first\n");
				
				return;
			}
			
			token = strtok(NULL, " ");
			char tablePath[1000];
			
			sprintf(tablePath, "./databases/%s/%s.txt", databaseUsed, token);
			// printf("%s\n", tablePath);
			
			// cek if table file exist
			if (access(tablePath	, F_OK) == 0) {
				// printf("File exists.\n");
			} else {
				sprintf(response, "1 table does not exist.\n");
				return;
			}
			
			int status = remove(tablePath);
    
			if (status == 0) {
				sprintf(response, "0 table deleted successfully.\n");
				logCommand(userQuery, databaseUsed, command);
			} else {
				sprintf(response, "1 Failed to delete the table.\n");
			}
			
			return;
		}
	}
	
	// DUMP 
	if(strcmp(token, "DUMP") == 0){
		char databasePath[300];
		sprintf(databasePath, "./databases/%s", databaseUsed);
		
		DIR *directory;
		struct dirent *entry;

		// Open the directory
		directory = opendir(databasePath);

		if (directory == NULL) {
		    sprintf(response, "1 Failed to open the database.\n");
		    return;
		}
		
		sprintf(response, "0 ");
		// Read the directory entries
		while ((entry = readdir(directory)) != NULL) {
		    // Exclude "." and ".." entries
		    if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
		    	char tableName[40];
		    	strcpy(tableName, entry->d_name);
		    	tableName[strlen(tableName)-4] = '\0';
		    	
		    	if(strcmp(tableName, "access")== 0 ){
		    		continue;
		    	}
		    	
		    	strcat(response, "DROP TABLE ");
		    	strcat(response, tableName);
		    	strcat(response, "\n");
		    	
		    	strcat(response, "CREATE TABLE ");
		    	strcat(response, tableName);
		    	
		    	// get attributes
		    	char filePath[500];
		    	sprintf(filePath, "./databases/%s/%s", databaseUsed, entry->d_name);
		    	FILE* file;
				char line[300];

				// Open the file
				file = fopen(filePath, "r");

				if (file == NULL) {
					sprintf(response, "1 Failed to open the file.\n");
					return;
				}

				// Read lines from the file
				fgets(line, sizeof(line), file);
				if(strlen(line)<1){
					strcat(response, "\n\n");
					continue;
				}
				
				strcat(response, " (");

				// Close the file
				fclose(file);
				
		    	char* attr;

				// Get the first word
				attr = strtok(line, "\t");
				
				char columnName[300];
				char columnType[300];

				// Find the opening bracket
				char* openingBracket = strchr(attr, '[');
				if (openingBracket == NULL) {
					printf("Opening bracket not found.\n");
					return;
				}

				// Find the closing bracket
				char* closingBracket = strchr(openingBracket, ']');
				if (closingBracket == NULL) {
					printf("Closing bracket not found.\n");
					return;
				}

				// Calculate the lengths of the words
				size_t columnNameLength = openingBracket - attr;
				size_t columnTypeLength = closingBracket - openingBracket - 1;

				// Extract the words
				strncpy(columnName, attr, columnNameLength);
				columnName[columnNameLength] = '\0';

				strncpy(columnType, openingBracket + 1, columnTypeLength);
				columnType[columnTypeLength] = '\0';

				// Print the extracted words
				// printf("First word: %s\n", columnName);
				// printf("Second word: %s\n", columnType);
				
				strcat(response, columnName);
				strcat(response, " ");
				strcat(response, columnType);

				// Parse the remaining words
				while (attr = strtok(NULL, "\t")) {
					removeNewLine(attr);
					if(strlen(attr)<1) break;
					
					char columnName[300];
					char columnType[300];

					// Find the opening bracket
					char* openingBracket = strchr(attr, '[');
					if (openingBracket == NULL) {
						printf("Opening bracket not found.\n");
						return;
					}

					// Find the closing bracket
					char* closingBracket = strchr(openingBracket, ']');
					if (closingBracket == NULL) {
						printf("Closing bracket not found.\n");
						return;
					}

					// Calculate the lengths of the words
					size_t columnNameLength = openingBracket - attr;
					size_t columnTypeLength = closingBracket - openingBracket - 1;

					// Extract the words
					strncpy(columnName, attr, columnNameLength);
					columnName[columnNameLength] = '\0';

					strncpy(columnType, openingBracket + 1, columnTypeLength);
					columnType[columnTypeLength] = '\0';

					// Print the extracted words
					// printf("First word: %s\n", columnName);
					// printf("Second word: %s\n", columnType);
				
					strcat(response, ", ");
					strcat(response, columnName);
					strcat(response, " ");
					strcat(response, columnType);
				}

		    	strcat(response, ")");
		    	strcat(response, "\n\n");
		    	
		        // printf("DROP TABLEFile name: %s\n", entry->d_name);
		    }
		}

		// Close the directory
		closedir(directory);
		
		sprintf(response, "%s", response);
		return;
	}
	
	
	sprintf(response, "1 Error syntax\n");
	return;
	
	sprintf(response, "1 Error syntax\n");
	return;
}

int main() {
	loadUserAndPasswd();
	
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int addrlen = sizeof(address);
    
    char *response = "Hello from server!";

    // Create socket file descriptor
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    // Set socket options
    int opt = 1;
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("Setsockopt failed");
        exit(EXIT_FAILURE);
    }

    // Configure server address
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    // Bind the socket to the specified IP and port
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("Binding failed");
        exit(EXIT_FAILURE);
    }

    // Listen for connections
    if (listen(server_fd, 3) < 0) {
        perror("Listen failed");
        exit(EXIT_FAILURE);
    }
    
    // Accept incoming connection
    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
        perror("Accept failed");
        exit(EXIT_FAILURE);
    }

    while (1) {
        char message[2*BUFFER_SIZE+100] = {0};
   		char query[BUFFER_SIZE] = {0};
        
        // Read data from the client
        // FORMAT: "isRoot user query"
        valread = read(new_socket, message, 2*BUFFER_SIZE + 100);
        // printf("%s\n", message);
		
		char temp[3000];
		strcpy(temp, message);
		
		char *token = strtok(temp, " ");
		if(token == NULL) return 0;
		
		isRoot = atoi(token);
		
		token = strtok(NULL, " ");
		char userName[50];
		strcpy(userName, token);
		
		token = strtok(NULL, " ");
		char databaseUsed[100];
		strcpy(databaseUsed, token);
		
		token = strtok(NULL, " ");
		while(token != NULL){
			strcat(query, token);
			strcat(query, " ");
			token = strtok(NULL, " ");
		}
		
		// printf("%s\n", query);
		char response[10000] = {0};
		
		process(query, response, userName, databaseUsed);
		
        // Send response to the client
        // FORMAT: "signal message"
        send(new_socket, response, strlen(response), 0);
     
        query[0] = '\0';
        response[0] = '\0';
    }
	
    // Close the server socket
    close(new_socket);
    close(server_fd);

    return 0;
}