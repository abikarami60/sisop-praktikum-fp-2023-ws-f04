#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 8080
#define BUFFER_SIZE 1024

#define MAX_QUERY_LEN 1000

int isRoot = 0;
char databaseUsed[100] = "NoDatabase";

int main(int argc, char *argv[]){
	for(int i=0;i<argc;i++){
		// printf("%d: %s\n", i, argv[i]);
	}

	
	// cek apakah program diakses root
	uid_t uid = geteuid();
    
	if (uid == 0) {
	    // printf("Program executed by root.\n");
	    isRoot = 1;
	}
	
	// cek jumlah argumen 
	if(argc != 5 && !(isRoot)){
		printf("Jumlah argumen tidak tepat (harus 5)\n");
		return 0;
	}
	
	// setting socket 
	int sock = 0, valread;
	struct sockaddr_in serv_addr;
	char *message = "Hello from client!";


	// Create socket file descriptor
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
	perror("Socket creation failed");
	exit(EXIT_FAILURE);
	}

	// Configure server address
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);

	// Convert IP address from text to binary form
	if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
	perror("Invalid address or address not supported");
	exit(EXIT_FAILURE);
	}

	// Connect to the server
	if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
	perror("Connection failed");
	exit(EXIT_FAILURE);
	}

	char *user;
	char *pass;
	
	// authentication
	if(!isRoot){
		char buffer[BUFFER_SIZE] = {0};
		user = argv[2];
		pass = argv[4];
		int signal;
		char authMessage[200];
		sprintf(authMessage, "%d %s %s VERIFY %s %s", isRoot, user, databaseUsed, user, pass);
		send(sock, authMessage, strlen(authMessage), 0);
		// printf("Message sent to server\n");
	
		// receice response
		valread = read(sock, buffer, BUFFER_SIZE);
		// printf("%s", buffer);

		char temp[1024];
		strcpy(temp, buffer);
		char *token = strtok(temp, " ");
		signal = atoi(token);
    	
		token = strtok(NULL, " ");
		if(signal != 0){
			char errorMsg[100] = {0};
			while(token != NULL){
					strcat(errorMsg, token);
					strcat(errorMsg, " ");
					token = strtok(NULL, " ");
				}
				printf("%s", errorMsg);
				return 0;
    		} else {
    			printf("OK\n");
    		}
	} else {
		char *root = "root";
		user = root;
	}
	
	// Query
	// char query[MAX_QUERY_LEN];
	while(1){
		char buffer[BUFFER_SIZE] = {0};
		char query[MAX_QUERY_LEN] = {0};
		fgets(query, 1000, stdin);
		query[strlen(query)-1]='\0';
		if(strcmp(query, "EXIT")==0){
			break;
		}
		
		// USE database
		char temp1[1000];
		strcpy(temp1, query);

		char* space = strchr(temp1, ' ');
		int spaceLength = space - temp1;

		char firstWord[spaceLength + 1]; // +1 for the null terminator
		strncpy(firstWord, temp1, spaceLength);
		firstWord[spaceLength] = '\0'; // Null terminate the string
        
		char message[3000] = {0};
		sprintf(message, "%d %s %s %s", isRoot, user, databaseUsed, query);
		
		if(strcmp(firstWord, "USE") == 0){
			send(sock, message, strlen(message), 0);
			// printf("USE Message sent to server\n");

			valread = read(sock, buffer, BUFFER_SIZE);
			int signal = 0; 
			char temp[1024] = {0};
			strcpy(temp, buffer);
			
			char* firstSpace = strchr(temp, ' ');
			int firstWordLength = firstSpace - temp;
			
			char signalStr[firstWordLength + 1]; // +1 for the null terminator
			strncpy(signalStr, temp, firstWordLength);
			signalStr[firstWordLength] = '\0'; // Null terminate the string
			signal = atoi(signalStr);
		    
			char responseMessage[strlen(firstSpace + 1) + 1]; // +1 for the null terminator
			strcpy(responseMessage, firstSpace + 1);
			
			
			if(signal != 0){
				printf("Error message: %d %s", signal, responseMessage);
				responseMessage[0] = '\0';
			} else {
				printf("Succes Message: %d %s\n", signal, responseMessage);
				strcpy(databaseUsed, responseMessage);
				responseMessage[0] = '\0';
			};
			
        	continue;
		}
			
		// printf("QUERY: %s\n", query);
		// send query
		// FORMAT: "isRoot user query"
		send(sock, message, strlen(message), 0);
    	// printf("Message sent to server\n");

		// receice response
		// FORMAT: "signal message"
		valread = read(sock, buffer, BUFFER_SIZE);
    	
		int signal = 0; 
		char temp[1024] = {0};
		strcpy(temp, buffer);

		char* firstSpace = strchr(temp, ' ');
		int firstWordLength = firstSpace - temp;

		char signalStr[firstWordLength + 1]; // +1 for the null terminator
		strncpy(signalStr, temp, firstWordLength);
		signalStr[firstWordLength] = '\0'; // Null terminate the string
		signal = atoi(signalStr);

		char responseMessage[strlen(firstSpace + 1) + 1]; // +1 for the null terminator
		strcpy(responseMessage, firstSpace + 1);
    
		if(signal != 0){
				printf("Error message: %d %s", signal, responseMessage);
				responseMessage[0] = '\0';
		} else {
				printf("Succes Message: %d %s", signal, responseMessage);
				responseMessage[0] = '\0';
		}
    	
	}

	close(sock);

	return 0;
}