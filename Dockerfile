# Base image
FROM ubuntu:latest



# Install dependencies
RUN apt-get update && apt-get install -y gcc make


# Set working directory
WORKDIR /app



# Copy source code
COPY database/ /app/database/
COPY client/ /app/client/
COPY dump/ /app/dump/


# Compile database program
RUN gcc -o /app/database/database /app/database/database.c



# Compile client program
RUN gcc -o /app/client/client /app/client/client.c



# Compile dump program
RUN gcc -o /app/dump/client_dump /app/dump/client_dump.c



# Set entrypoint command
CMD ["/bin/bash"]