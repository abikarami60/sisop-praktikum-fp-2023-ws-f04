# sisop-praktikum-fp-4-2023-WS-F04
# Praktikum Sistem Operasi Modul 4
# Kelompok F04
- Fathan Abi Karami 5025211156
- Heru Dwi Kurniawan 5025211055
- Alya Putri Salma 5025211174



# Setting socket dan inisiasi
Melakukan inisiasi.

## server.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <dirent.h>
#include <time.h>

#define PORT 8080
#define BUFFER_SIZE 1024

int isRoot = 0;

char user[100][100];
char pass[100][100];
int userCount = 0;

// fungsi load user dan password
void loadUserAndPasswd(){
	userCount=0;
	FILE *file;
    char line[1000];

    // Open the file in read mode
    file = fopen("user.txt", "r");

    if (file == NULL) {
        system("touch user.txt");
        //printf("Failed to open the file.\n");
        return;
    }
    
    char tempString[1000];

    // Read a line from the file
    while(fgets(line, 1000, file) != NULL){
    	strcpy(tempString, line);
    	char* token = strtok(tempString, " ");
    	if (token != NULL) {
		    strcpy(user[userCount], token);

		    // Get the second word
		    token = strtok(NULL, " ");
		    if (token != NULL) {
		        strcpy(pass[userCount], token);
		    }
		}
		userCount++;
    }

    // Close the file
    fclose(file);
}

int main() {
    // load user dan password untuk autentikasi
	loadUserAndPasswd();
	

    // setting socket
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int addrlen = sizeof(address);
    
    char *response = "Hello from server!";

    // Create socket file descriptor
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    // Set socket options
    int opt = 1;
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("Setsockopt failed");
        exit(EXIT_FAILURE);
    }

    // Configure server address
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    // Bind the socket to the specified IP and port
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("Binding failed");
        exit(EXIT_FAILURE);
    }

    // Listen for connections
    if (listen(server_fd, 3) < 0) {
        perror("Listen failed");
        exit(EXIT_FAILURE);
    }
    
    // Accept incoming connection
    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
        perror("Accept failed");
        exit(EXIT_FAILURE);
    }
}
```
melakukan load user dan password dati file user.txt. kemudian membuat socket 
## client.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 8080
#define BUFFER_SIZE 1024

#define MAX_QUERY_LEN 1000

int isRoot = 0;
char databaseUsed[100] = "NoDatabase";

int main(int argc, char *argv[]){
	for(int i=0;i<argc;i++){
		// printf("%d: %s\n", i, argv[i]);
	}

	
	// cek apakah program diakses root
	uid_t uid = geteuid();
    
	if (uid == 0) {
	    // printf("Program executed by root.\n");
	    isRoot = 1;
	}
	
	// cek jumlah argumen 
	if(argc != 5 && !(isRoot)){
		printf("Jumlah argumen tidak tepat (harus 5)\n");
		return 0;
	}
	
	// setting socket 
	int sock = 0, valread;
	struct sockaddr_in serv_addr;
	char *message = "Hello from client!";


	// Create socket file descriptor
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
	perror("Socket creation failed");
	exit(EXIT_FAILURE);
	}

	// Configure server address
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);

	// Convert IP address from text to binary form
	if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
	perror("Invalid address or address not supported");
	exit(EXIT_FAILURE);
	}

	// Connect to the server
	if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
	perror("Connection failed");
	exit(EXIT_FAILURE);
	}
}
```
Cek user (apakah root atau tidak). cek jumlah argumen. setting socket dan connect ke socket

# Mekanisme Send dan Receive
client akan mengirimkan 1 message dan menerima 1 message

server menerima 1 message dan mengerimkan kembali 1 message

## client.c
```c
while(1){
		char buffer[BUFFER_SIZE] = {0};
		char query[MAX_QUERY_LEN] = {0};
		fgets(query, 1000, stdin);
		query[strlen(query)-1]='\0';
		if(strcmp(query, "EXIT")==0){
			break;
		}
		
		// USE database
		char temp1[1000];
		strcpy(temp1, query);

		char* space = strchr(temp1, ' ');
		int spaceLength = space - temp1;

		char firstWord[spaceLength + 1]; // +1 for the null terminator
		strncpy(firstWord, temp1, spaceLength);
		firstWord[spaceLength] = '\0'; // Null terminate the string
        
		char message[3000] = {0};
		sprintf(message, "%d %s %s %s", isRoot, user, databaseUsed, query);
		
		if(strcmp(firstWord, "USE") == 0){
			send(sock, message, strlen(message), 0);
			// printf("USE Message sent to server\n");

			valread = read(sock, buffer, BUFFER_SIZE);
			int signal = 0; 
			char temp[1024] = {0};
			strcpy(temp, buffer);
			
			char* firstSpace = strchr(temp, ' ');
			int firstWordLength = firstSpace - temp;
			
			char signalStr[firstWordLength + 1]; // +1 for the null terminator
			strncpy(signalStr, temp, firstWordLength);
			signalStr[firstWordLength] = '\0'; // Null terminate the string
			signal = atoi(signalStr);
		    
			char responseMessage[strlen(firstSpace + 1) + 1]; // +1 for the null terminator
			strcpy(responseMessage, firstSpace + 1);
			
			
			if(signal != 0){
				printf("Error message: %d %s", signal, responseMessage);
				responseMessage[0] = '\0';
			} else {
				printf("Succes Message: %d %s\n", signal, responseMessage);
				strcpy(databaseUsed, responseMessage);
				responseMessage[0] = '\0';
			};
			
        	continue;
		}
			
		// printf("QUERY: %s\n", query);
		// send query
		// FORMAT: "isRoot user query"
		send(sock, message, strlen(message), 0);
    	// printf("Message sent to server\n");

		// receice response
		// FORMAT: "signal message"
		valread = read(sock, buffer, BUFFER_SIZE);
    	
		int signal = 0; 
		char temp[1024] = {0};
		strcpy(temp, buffer);

		char* firstSpace = strchr(temp, ' ');
		int firstWordLength = firstSpace - temp;

		char signalStr[firstWordLength + 1]; // +1 for the null terminator
		strncpy(signalStr, temp, firstWordLength);
		signalStr[firstWordLength] = '\0'; // Null terminate the string
		signal = atoi(signalStr);

		char responseMessage[strlen(firstSpace + 1) + 1]; // +1 for the null terminator
		strcpy(responseMessage, firstSpace + 1);
    
		if(signal != 0){
				printf("Error message: %d %s", signal, responseMessage);
				responseMessage[0] = '\0';
		} else {
				printf("Succes Message: %d %s", signal, responseMessage);
				responseMessage[0] = '\0';
		}
    	
	}

	close(sock);
```
mengirimkan message dengan format "isRoot user databaseUsed query"
1. get input
2. create string message dengan format ("isRoot user databaseUsed query")
3. send message

dan menerima message dengan format "signal reponseMessage"

1. receive message
2. parse message (signal and message)
3. tampilkan message berdasarkan jenis signalnya

terdapat 2 case yaitu case ketika menggunakan "USE" dan tidak. Case menggunakan USE akan mengubah databaseUsed, case selain itu tidak mengganti databaseUsed, hanya menampilkan message

## server.c
```c
while (1) {
        char message[2*BUFFER_SIZE+100] = {0};
   		char query[BUFFER_SIZE] = {0};
        
        // Read data from the client
        // FORMAT: "isRoot user query"
        valread = read(new_socket, message, 2*BUFFER_SIZE + 100);
        // printf("%s\n", message);
		
		char temp[3000];
		strcpy(temp, message);
		
		char *token = strtok(temp, " ");
		if(token == NULL) return 0;
		
		isRoot = atoi(token);
		
		token = strtok(NULL, " ");
		char userName[50];
		strcpy(userName, token);
		
		token = strtok(NULL, " ");
		char databaseUsed[100];
		strcpy(databaseUsed, token);
		
		token = strtok(NULL, " ");
		while(token != NULL){
			strcat(query, token);
			strcat(query, " ");
			token = strtok(NULL, " ");
		}
		
		// printf("%s\n", query);
		char response[10000] = {0};
		
		process(query, response, userName, databaseUsed);
		
        // Send response to the client
        // FORMAT: "signal message"
        send(new_socket, response, strlen(response), 0);
     
        query[0] = '\0';
        response[0] = '\0';
    }
```
server menerima message
1. menerima message dengan format "isRoot user databaseUsed query"
2. parsing message, dapatkan isRoot, user, databaseUSed, dan query
3. process query
4. send response message ke client

# POIN A: Autentikasi
## Verify user and password
### client.c 
```c
char *user;
	char *pass;
	
	// authentication
	if(!isRoot){
		char buffer[BUFFER_SIZE] = {0};
		user = argv[2];
		pass = argv[4];
		int signal;
		char authMessage[200];
		sprintf(authMessage, "%d %s %s VERIFY %s %s", isRoot, user, databaseUsed, user, pass);
		send(sock, authMessage, strlen(authMessage), 0);
		// printf("Message sent to server\n");
	
		// receice response
		valread = read(sock, buffer, BUFFER_SIZE);
		// printf("%s", buffer);

		char temp[1024];
		strcpy(temp, buffer);
		char *token = strtok(temp, " ");
		signal = atoi(token);
    	
		token = strtok(NULL, " ");
		if(signal != 0){
			char errorMsg[100] = {0};
			while(token != NULL){
					strcat(errorMsg, token);
					strcat(errorMsg, " ");
					token = strtok(NULL, " ");
				}
				printf("%s", errorMsg);
				return 0;
    		} else {
    			printf("OK\n");
    		}
	} else {
		char *root = "root";
		user = root;
	}
```
jika bukan root client melakukan autentikasi mengirim message: VERIFY user password, ke server.c

message diterima dan dicek jika error maka exit client

jika root maka ubah user menjadi root

### server.c
```c
void process(char command[BUFFER_SIZE], char response[10000], char userQuery[50], char databaseUsed[100]){
	response[0] = '\0';  // response message
	int signal = 0;      // signal (1 = erorr, 0 = no error)
	
	char temp[BUFFER_SIZE];
	strcpy(temp, command);
	
	char *token = strtok(temp, " ");
	
	// Error Case Query Kosong
	if(token == NULL){
		sprintf(response, "1 Query tidak ada");
		return;
	} 
	
	// Authenticate user
	if(strcmp(token, "VERIFY") == 0){
		char *userName = strtok(NULL, " ");
		char *passwd = strtok(NULL, " ");
		
		// printf("%d\n", userCount);
		
		int found = 0;
		int wrongPass = 1;
		for(int i = 0; i<userCount+2;i++){
			if(strcmp(userName, user[i])==0){
				found = 1;
				if(strcmp(passwd, pass[i])==-10){
					wrongPass=0;
				}
			}
		}
		
		if(!found){
			signal = 1;
			sprintf(response, "%d User not found\n", signal);
			
			return;
		}
		
		if(wrongPass){
			signal = 1;
			sprintf(response, "%d Wrong Password\n", signal);
			
			return;
		}
		
		sprintf(response, "%d WELCOME %s\n", signal, userName);
		
		return;
	}
}
```
pada saat processing query jika word pertama "VERIFY" maka lakukan verify.
1. get user and password
2. bandingkan dengan user dan pass yang sudah di load
3. jika password salah maka beri message error
4. jika benar beri message success
5. jika user tidak ada beri message user not found

## Create User
### server.c
pada fungsi process()
```c
if(strcmp(token, "CREATE") == 0){
	token = strtok(NULL, " ");
		
	// CREATE USER
	if(strcmp(token, "USER") == 0){
	
		// cek apakah root
		if(strcmp(userQuery, "root")!=0){
			signal = 1;
			response[0] = '\0';
			sprintf(response, "%d Only root can CREATE USER\n", signal);
			return;
		}
		
		char *userName = strtok(NULL, " ");
		
		// cek apakah user already exist
		for(int i = 0; i<userCount;i++){
			if(strcmp(userName, user[i]) == 0){
				signal = 1;
				sprintf(response, "%d Username already exist\n", signal);
				
				return;
			}
		}
		
		token = strtok(NULL, " ");
		if(strcmp(token, "IDENTIFIED") != 0){
			sprintf(response, "1 Error syntax\n");
			
			return;
		} 
		
		token = strtok(NULL, " ");
		if(strcmp(token, "BY") != 0){
			sprintf(response, "1 Error syntax\n");
			
			return;
		} 
		
		char *passwd = strtok(NULL, " ");
		
		FILE *file;
		char temp[1000];
		sprintf(temp, "%s %s", userName, passwd);

		// Open the file in append mode
		file = fopen("user.txt", "a");

		if (file == NULL) {
			sprintf(response, "1 Failed to open user.txt.\n");
			return;
		}

		// Write the new line of text into the file
		fprintf(file, "%s\n", temp);

		// Close the file
		fclose(file);
		
		// printf("Succesfully created user\n");
		sprintf(response, "%d Succesfully created user\n", signal);
		logCommand(userQuery, databaseUsed, command);
		
		loadUserAndPasswd();
		
		return;	
	}
		
}
```
1. cek apakah root
2. cek apakah user already exist
3. tambahkan ke user.txt
4. load kembali user dan password
5. response berhasil

## Output
terminal

![](./img/A_terminal.png)
![](./img/A_terminal2.png)

user.txt

![](./img/A_usertxt.png)


# POIN B: Autorisasi
## USE database
### server.c
pada fungsi process()
```c
if(strcmp(token, "USE")==0){
		char *databaseNameUse = strtok(NULL, " ");
		char accessTxt[1000];
		sprintf(accessTxt, "./databases/%s/access.txt", databaseNameUse);
		
		FILE* file = fopen(accessTxt, "r");
		
		// Error Case database not found
		if (file == NULL) {
			sprintf(response, "1 database %s not found.\n", databaseNameUse);
			return;
		}

		// Check Apakah user memiliki akses atau user adalah root
		char names[100];
		while (fgets(names, sizeof(names), file) != NULL) {
			// Remove the newline character
			names[strcspn(names, "\n")] = '\0';

			if (strcmp(names, userQuery) == 0 || strcmp(userQuery, "root")==0) {
			    sprintf(response, "0 %s", databaseNameUse);
			    
			    logCommand(userQuery, databaseUsed, command);
			    fclose(file);
			    return;
			}
		}

		// Close the file
		fclose(file);
		
		// Case jika tidak memiliki akses
		sprintf(response, "1 You are not permitted to use this database.\n");

		return;
	}
```
1. cek apah database exist
2. buka access.txt pada folder database yang ingin digunakan
3. cek apakah user ada di access.txt atau merupakan root
4. jika ada atau merupakan root, response berhasil

### client.c
```c
if(strcmp(firstWord, "USE") == 0){
			send(sock, message, strlen(message), 0);
			// printf("USE Message sent to server\n");

			valread = read(sock, buffer, BUFFER_SIZE);
			int signal = 0; 
			char temp[1024] = {0};
			strcpy(temp, buffer);
			
			char* firstSpace = strchr(temp, ' ');
			int firstWordLength = firstSpace - temp;
			
			char signalStr[firstWordLength + 1]; // +1 for the null terminator
			strncpy(signalStr, temp, firstWordLength);
			signalStr[firstWordLength] = '\0'; // Null terminate the string
			signal = atoi(signalStr);
		    
			char responseMessage[strlen(firstSpace + 1) + 1]; // +1 for the null terminator
			strcpy(responseMessage, firstSpace + 1);
			
			
			if(signal != 0){
				printf("Error message: %d %s", signal, responseMessage);
				responseMessage[0] = '\0';
			} else {
				printf("Succes Message: %d %s\n", signal, responseMessage);
				strcpy(databaseUsed, responseMessage);
				responseMessage[0] = '\0';
			};
			
        	continue;
		}
```
diterima oleh client.c dan mengubah databaseUSed pada client.c

## GRANT PERMISSION

### server.c
```c
if(strcmp(token, "GRANT")==0){
		token = strtok(NULL, " ");
		if(strcmp(token, "PERMISSION")==0){
			if(strcmp(userQuery, "root")!=0){
				signal = 1;
				response[0] = '\0';
				sprintf(response, "%d Only root can GRANT PERMISSION\n", signal);
				return;
			}
			
			char *databaseName = strtok(NULL, " ");
			
			// Cek apakah database exist
			char databasePath[1000];
			sprintf(databasePath, "./databases/%s", databaseName);
			DIR *dir = opendir(databasePath);
    
			if (dir) {
				// printf("Directory exists.\n");
				closedir(dir);
			} else {
				sprintf(response, "1 database does not exist.\n");
				
				return;
			}
			
			// error invalid syntax
			token = strtok(NULL, " ");
			if(strcmp(token, "INTO") != 0){
				sprintf(response, "1 error syntax\n");
				return;
			} 
			
			char *userAccess = strtok(NULL, " ");
			
			// Cek apakah user exist
			int found =0;
			for(int i = 0; i<userCount+2;i++){
				if(strcmp(userAccess, user[i])==0){
					found = 1;
					break;
				}
			}
			
			// jika user tidak ada
			if(!found){
				signal = 1;
				sprintf(response, "%d User not found\n", signal);
				
				return;
			}
			
			char accessTxt[1000];
			sprintf(accessTxt, "./databases/%s/access.txt", databaseName);
			
			FILE* file = fopen(accessTxt, "r");
			if (file == NULL) {
				sprintf(response, "1 Failed to open the file access.txt.\n");
				return;
			}

			// Check apakah user sudah memiliki akses
			char names[100];
			while (fgets(names, sizeof(names), file) != NULL) {
				// Remove the newline character
				names[strcspn(names, "\n")] = '\0';
				
				if (strcmp(names, userAccess) == 0) {
				    sprintf(response, "1 user already has access\n");
				    fclose(file);
				    return;
				}
			}

			// Close the file
			fclose(file);
			
			file = fopen(accessTxt, "a");
			if (file == NULL) {
				sprintf(response, "1 Failed to open the file access.txt.\n");
				return;
			}

			// Append the name to the file
			fprintf(file, "%s\n", userAccess);
			sprintf(response, "0 Permission Granted.\n");
			logCommand(userQuery, databaseUsed, command);
			// Close the file
			fclose(file);
			
			return;
		} 
		
		sprintf(response, "1 Error Syntax\n");
	
		return;
	}
```
1. cek apakah root
2. cek apakah database exist
3. cek apakah user belum memiliki akses
4. jika ya pada ketiga poin diatas tambahkan user ke access.txt

## Output
terminal

![](./img/B_terminal.png)
![](./img/B_terminal2.png)

databases

![](./img/B_databases.png)



# POIN C: DDL

## CREATE DATABASE
### database.c
```c
if(strcmp(token, "DATABASE") == 0){
			
			token = strtok(NULL, " ");
			char databaseName[100];
			sprintf(databaseName, "./databases/%s", token);
			
			struct stat st;
			
			// cek apakah database sudah ada
			if (stat(databaseName, &st) == 0 && S_ISDIR(st.st_mode)) {
				response[0] = '\0';
				sprintf(response, "1 Database already exists.\n");
				
				return;
			} 
			
			// Create the directory
			int result = mkdir(databaseName, 0777); // Set the appropriate permissions here
			
			if (result == 0) {
				response[0] = '\0';
			    sprintf(response, "0 Database created successfully.\n");
			    logCommand(userQuery, databaseUsed, command);
			    // printf("%s", response);
			} else {
				response[0] = '\0';
			    sprintf(response, "1 Failed to create database.\n");
			    // printf("%s", response);
			}
			
			// buat file access.txt dan masukkan username ke acces.txt
			char command[1000];
			sprintf(command, "cd %s && touch access.txt", databaseName);
			system(command);
			char access[1000];
			sprintf(access, "%s/access.txt", databaseName);
			
			FILE *file;
			
			file = fopen(access, "a");

			if (file == NULL) {
				sprintf(response, "1 Failed to open the acces.txt.\n");
				return;
			}

			// Write the new line of text into the file
			fprintf(file, "%s\n", userQuery);

			// Close the file
			fclose(file);
			
			return;
		}
```
1. get databse name
2. cek apakah databse sudah ada
3. create direktori database
4. buat file akses
5. tambahkan nama user yang create database ke access.txt


## CREATE TABLE
database.c
```c
// CREATE TABLE 
		if(strcmp(token, "TABLE") == 0){
			// cek apakah sudah memilih database
			if(strcmp(databaseUsed, "NoDatabase")==0){
				sprintf(response, "1 please select the database first\n");
				
				return;
			}
			
			token = strtok(NULL, " ");
			char tableName[100];
			strcpy(tableName, token);
			
			// CREATE TABLE FILE
			char filePath[500];
			sprintf(filePath, "./databases/%s/%s.txt", databaseUsed, tableName);
			// Check if the file exists
			if (access(filePath, F_OK) != -1) {
				sprintf(response, "1 table already exists: %s\n", tableName);
				return ;
			}

			FILE *file = fopen(filePath, "w");
			if (file == NULL) {
				sprintf(response, "1 Unable to create the table.\n");
				return ;
			}

			// Close the file
			fclose(file);
			
			// PROCESS ATTRIBUTE
			
			// extract from brackets
			char temp[1000];
			strcpy(temp, command);
			
			char *start_bracket = strchr(temp, '(');
			char *end_bracket = strchr(temp, ')');
			char attr[1000];

			if (start_bracket != NULL && end_bracket != NULL) {
				// Calculate the length of the substring between the brackets
				int length = end_bracket - start_bracket - 1;

				// Allocate memory for the substring
				char *substring = malloc((length + 1) * sizeof(char));

				// Copy the substring between the brackets
				strncpy(substring, start_bracket + 1, length);
				substring[length] = '\0';

				// printf("Substring: %s\n", substring);
				strcpy(attr, substring);
				// Free the allocated memory
				
			} else {
				sprintf(response, "1 table created but failed to assign attribute: please specify the attribute.\n");
				logCommand(userQuery, databaseUsed, command);
				return;
			}
			
			// apply attributes
			char header[1000];
			char *word;

			// Get the first token
			word = strtok(attr, " ");
			strcpy(header, word);
			// printf("%s\n", word);
			
			word = strtok(NULL, ",");
			strcat(header, "[");
			strcat(header, word);
			strcat(header, "]\t");
			// printf("%s\n", word);
			
			// Iterate through the remaining tokens
			while (word = strtok(NULL, " ")) {
				strcat(header, word);
				// printf("Word: %s\n", word);

				// Get the next token
				word = strtok(NULL, ",");
				strcat(header, "[");
				strcat(header, word);
				strcat(header, "]\t");
				// printf("Word: %s\n", word);
			}
			// printf("%s\n", header);
			
			// add to file
			// Open the file in append mode ("a")
			file = fopen(filePath, "a");

			if (file == NULL) {
				logCommand(userQuery, databaseUsed, command);
				sprintf(response, "1 table created but failed to assign attribut: Unable to open the table file.\n");
				return ;
			}

			// Write the line to the file
			fprintf(file, "%s\n", header);

			// Close the file
			fclose(file);

			// Succesfully created file and assigned attribute 
			sprintf(response, "0 table created: %s and succesfully assigned attribute\n", tableName);
			logCommand(userQuery, databaseUsed, command);
			
			return;
		}
		
		sprintf(response, "1 Error Syntax\n");
		
		return;
	
```
1. cek apakah sudah USE database
2. get table name
3. cek apakah table exist
4. create table file
5. process atribut
6. write atribut ke table file

## DROP DATABASE
### database.c
```c
if(strcmp(token, "DROP") == 0){
		token = strtok(NULL, " ");
		
		// DROP DATABASE
		if(strcmp(token, "DATABASE") == 0){
			token = strtok(NULL, " ");
			char databasePath[1000];
			
			sprintf(databasePath, "./databases/%s", token);
			// printf("%s\n", databasePath);
			
			DIR *dir = opendir(databasePath);
    		
    		// cek if database exist
			if (dir == NULL) {
				sprintf(response, "1 Database not found.\n");
				return;
			}
			
			struct dirent *entry;
			
			// Delete all file within database
			while ((entry = readdir(dir)) != NULL) {
				if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
				    char file_path[2000];
				    snprintf(file_path, sizeof(file_path), "%s/%s", databasePath, entry->d_name);
				    
				    if (unlink(file_path) != 0) {
				        sprintf(response, "1 Failed to delete file: %s\n", file_path);
				    }
				}
			}
			
			closedir(dir);
			
			// delete database
			int status = rmdir(databasePath);
				
			if (status == 0) {
				sprintf(response, "0 Succesfully deleted database.\n");
				logCommand(userQuery, databaseUsed, command);
			} else {
				sprintf(response, "1 Failed to delete the database.\n");
			}
			
			return;
		}
}
```
1. cek apakah database ada
2. delete all files pada direktori database
3. delete direktori

## DROP TABLE
```c
if(strcmp(token, "TABLE") == 0){
			// cek apakah sudah memilih database
			if(strcmp(databaseUsed, "NoDatabase")==0){
				sprintf(response, "1 please select the database first\n");
				
				return;
			}
			
			token = strtok(NULL, " ");
			char tablePath[1000];
			
			sprintf(tablePath, "./databases/%s/%s.txt", databaseUsed, token);
			// printf("%s\n", tablePath);
			
			// cek if table file exist
			if (access(tablePath	, F_OK) == 0) {
				// printf("File exists.\n");
			} else {
				sprintf(response, "1 table does not exist.\n");
				return;
			}
			
			int status = remove(tablePath);
    
			if (status == 0) {
				sprintf(response, "0 table deleted successfully.\n");
				logCommand(userQuery, databaseUsed, command);
			} else {
				sprintf(response, "1 Failed to delete the table.\n");
			}
			
			return;
		}
	
```
1. cek apakah sudah use database
2. cek apakah table file exist
3. delete table file

## Output
### create database and table:
terminal 

![](./img/C_terminal.png)
![](./img/C_terminal2.png)

databases:

![](./img/C_databases.png)

officeDB:

![](./img/C_officeDB.png)

access.txt

![](./img/C_accesstxt.png)

employee.txt

![](./img/C_employeetxt.png)

### drop database and table
terminal

![](./img/C_terminal3.png)

before drop table

![](./img/C_officeDB_beforeDrop.png)

after drop table

![](./img/C_officeDB_afterDrop.png)

before drop database

![](./img/C_database_beforedrop.png)

after drop database

![](./img/C_databases_afterdrop.png)


# POIN D: DML

# POIN E: Logging
logging function pada database.c
```c
void logCommand(char userQuery[50], char databaseUsed[100], char command[3000]){
	// printf("%s %s %s\n", userQuery, databaseUsed, command);
	time_t rawtime;
    struct tm *timeinfo;
    char timestamp[20];

    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", timeinfo);
    
    char logLine[4000];
    
    sprintf(logLine, "(%s):%s:%s", timestamp, userQuery, command);
    logLine[strcspn(logLine, "\n")] = '\0';

    // printf("%s\n", logLine);
    
    FILE *file;

    // Open the file in append mode, creating it if it doesn't exist
    file = fopen("log.txt", "a");
    if (file == NULL) {
        printf("Error opening the file.\n");
        return;
    }

    // Write the line to the file
    fprintf(file, "%s\n", logLine);

    // Close the file
    fclose(file);
    // printf("Line added to log.txt successfully.\n");
}
```

## output
log.txt

![](./img/E_logtxt.png)

# POIN F: Reliability
## client_dump.c:
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 8080
#define BUFFER_SIZE 10000

int isRoot = 0;
char databaseUsed[100] = "NoDatabase";

int main(int argc, char* argv[]){
	for(int i=0;i<argc;i++){
		// printf("%d: %s\n", i, argv[i]);
	}
	
	uid_t uid = geteuid();
    
	if (uid == 0) {
	    // printf("Program executed by root.\n");
	    isRoot = 1;
	}
	
	// cek jumlah argumen 
	
	if(argc != 6 && !(isRoot)){
		printf("Jumlah argumen tidak tepat (harus 6)\n");
		return 0;
	}
	
	
	
	// setting socket 
	int sock = 0, valread;
	struct sockaddr_in serv_addr;
	char *message = "Hello from client!";


	// Create socket file descriptor
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
	perror("Socket creation failed");
	exit(EXIT_FAILURE);
	}

	// Configure server address
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);

	// Convert IP address from text to binary form
	if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
	perror("Invalid address or address not supported");
	exit(EXIT_FAILURE);
	}

	// Connect to the server
	if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
	perror("Connection failed");
	exit(EXIT_FAILURE);
	}

	char *user;
	char *pass;
	
	// authentication
	if(!isRoot){
		char buffer[BUFFER_SIZE] = {0};
		user = argv[2];
		pass = argv[4];
		int signal;
		char authMessage[200];
		sprintf(authMessage, "%d %s %s VERIFY %s %s", isRoot, user, databaseUsed, user, pass);
		send(sock, authMessage, strlen(authMessage), 0);
		// printf("Message sent to server\n");
	
		// receice response
		valread = read(sock, buffer, BUFFER_SIZE);
		// printf("%s", buffer);

		char temp[1024];
		strcpy(temp, buffer);
		char *token = strtok(temp, " ");
		signal = atoi(token);
    	
		token = strtok(NULL, " ");
		if(signal != 0){
			char errorMsg[100] = {0};
			while(token != NULL){
					strcat(errorMsg, token);
					strcat(errorMsg, " ");
					token = strtok(NULL, " ");
				}
				fprintf(stderr, "Error verifying user.\n");
				return 0;
    		} else {
    			// printf("OK\n");
    		}
	} else {
		char *root = "root";
		user = root;
	}
	
	// Akses
	char buffer[BUFFER_SIZE] = {0};
	char database[100];
	if(!isRoot){
		strcpy(database, argv[5]);
	} else {
		strcpy(database, argv[1]);
	}
	
	
	char accessMessage[500];
	sprintf(accessMessage, "%d %s %s USE %s", isRoot, user, databaseUsed, database);
	
	send(sock, accessMessage, strlen(accessMessage), 0);
	
	valread = read(sock, buffer, BUFFER_SIZE);
	
	int signal = 0; 
	char temp[10000] = {0};
	strcpy(temp, buffer);
	
	char* firstSpace = strchr(temp, ' ');
	int firstWordLength = firstSpace - temp;
	
	char signalStr[firstWordLength + 1]; // +1 for the null terminator
	strncpy(signalStr, temp, firstWordLength);
	signalStr[firstWordLength] = '\0'; // Null terminate the string
	signal = atoi(signalStr);
    
	char responseMessage[strlen(firstSpace + 1) + 1]; // +1 for the null terminator
	strcpy(responseMessage, firstSpace + 1);
	
	
	if(signal != 0){
		fprintf(stderr, "Error trying database.\n");
		return 0;
	} else {
		// printf("Succes Message: %d %s\n", signal, responseMessage);
		strcpy(databaseUsed, responseMessage);
	};
	
	buffer[0] = '\0';
	temp[0] = '\0';
	
	
	// dump
	signal = 0;
	char dumpMessage[500];
	sprintf(dumpMessage, "%d %s %s DUMP", isRoot, user, databaseUsed);
	
	send(sock, dumpMessage, strlen(dumpMessage), 0);
	
	valread = read(sock, buffer, BUFFER_SIZE);
	
	temp[10000] = '\0';
	strcpy(temp, buffer);
	
	firstSpace = strchr(temp, ' ');
	firstWordLength = firstSpace - temp;
	
	signalStr[firstWordLength + 1]; // +1 for the null terminator
	strncpy(signalStr, temp, firstWordLength);
	signalStr[firstWordLength] = '\0'; // Null terminate the string
	signal = atoi(signalStr);
	
	char dump[strlen(firstSpace + 1) + 1]; // +1 for the null terminator
	strcpy(dump, firstSpace + 1);
	
	if(signal != 0){
		printf("%d: %s\n", signal, dump);
		return 1;
	} else {
		printf("%s\n", dump);
		
	};
	
	
	return 0;
}
```
1. Setting socket
2. autentikasi
3. akses database (using USE)
4. Dump database (using DUMP)
5. print response

## database.c
```c
// DUMP 
	if(strcmp(token, "DUMP") == 0){
		char databasePath[300];
		sprintf(databasePath, "./databases/%s", databaseUsed);
		
		DIR *directory;
		struct dirent *entry;

		// Open the directory
		directory = opendir(databasePath);

		if (directory == NULL) {
		    sprintf(response, "1 Failed to open the database.\n");
		    return;
		}
		
		sprintf(response, "0 ");
		// Read the directory entries
		while ((entry = readdir(directory)) != NULL) {
		    // Exclude "." and ".." entries
		    if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
		    	char tableName[40];
		    	strcpy(tableName, entry->d_name);
		    	tableName[strlen(tableName)-4] = '\0';
		    	
		    	if(strcmp(tableName, "access")== 0 ){
		    		continue;
		    	}
		    	
		    	strcat(response, "DROP TABLE ");
		    	strcat(response, tableName);
		    	strcat(response, "\n");
		    	
		    	strcat(response, "CREATE TABLE ");
		    	strcat(response, tableName);
		    	
		    	// get attributes
		    	char filePath[500];
		    	sprintf(filePath, "./databases/%s/%s", databaseUsed, entry->d_name);
		    	FILE* file;
				char line[300];

				// Open the file
				file = fopen(filePath, "r");

				if (file == NULL) {
					sprintf(response, "1 Failed to open the file.\n");
					return;
				}

				// Read lines from the file
				fgets(line, sizeof(line), file);
				if(strlen(line)<1){
					strcat(response, "\n\n");
					continue;
				}
				
				strcat(response, " (");

				// Close the file
				fclose(file);
				
		    	char* attr;

				// Get the first word
				attr = strtok(line, "\t");
				
				char columnName[300];
				char columnType[300];

				// Find the opening bracket
				char* openingBracket = strchr(attr, '[');
				if (openingBracket == NULL) {
					printf("Opening bracket not found.\n");
					return;
				}

				// Find the closing bracket
				char* closingBracket = strchr(openingBracket, ']');
				if (closingBracket == NULL) {
					printf("Closing bracket not found.\n");
					return;
				}

				// Calculate the lengths of the words
				size_t columnNameLength = openingBracket - attr;
				size_t columnTypeLength = closingBracket - openingBracket - 1;

				// Extract the words
				strncpy(columnName, attr, columnNameLength);
				columnName[columnNameLength] = '\0';

				strncpy(columnType, openingBracket + 1, columnTypeLength);
				columnType[columnTypeLength] = '\0';

				// Print the extracted words
				// printf("First word: %s\n", columnName);
				// printf("Second word: %s\n", columnType);
				
				strcat(response, columnName);
				strcat(response, " ");
				strcat(response, columnType);

				// Parse the remaining words
				while (attr = strtok(NULL, "\t")) {
					removeNewLine(attr);
					if(strlen(attr)<1) break;
					
					char columnName[300];
					char columnType[300];

					// Find the opening bracket
					char* openingBracket = strchr(attr, '[');
					if (openingBracket == NULL) {
						printf("Opening bracket not found.\n");
						return;
					}

					// Find the closing bracket
					char* closingBracket = strchr(openingBracket, ']');
					if (closingBracket == NULL) {
						printf("Closing bracket not found.\n");
						return;
					}

					// Calculate the lengths of the words
					size_t columnNameLength = openingBracket - attr;
					size_t columnTypeLength = closingBracket - openingBracket - 1;

					// Extract the words
					strncpy(columnName, attr, columnNameLength);
					columnName[columnNameLength] = '\0';

					strncpy(columnType, openingBracket + 1, columnTypeLength);
					columnType[columnTypeLength] = '\0';

					// Print the extracted words
					// printf("First word: %s\n", columnName);
					// printf("Second word: %s\n", columnType);
				
					strcat(response, ", ");
					strcat(response, columnName);
					strcat(response, " ");
					strcat(response, columnType);
				}

		    	strcat(response, ")");
		    	strcat(response, "\n\n");
		    	
		        // printf("DROP TABLEFile name: %s\n", entry->d_name);
		    }
		}

		// Close the directory
		closedir(directory);
		
		sprintf(response, "%s", response);
		return;
	}
```
1. open database directory
2. itterate semua table file
3. print DROP TABLE
4. print CREATE TABLE
5. process attribut

## Output
terminal:

![](./img/F_terminal.png)

backup.txt:

![](./img/F_backuptxt.png)

# POIN G

# POIN H : Error Handling
1. cek jumlah arguman program client.c
```c
// cek jumlah argumen 
	if(argc != 5 && !(isRoot)){
		printf("Jumlah argumen tidak tepat (harus 5)\n");
		return 0;
	}
```
2. cek password
```c
if(wrongPass){
			signal = 1;
			sprintf(response, "%d Wrong Password\n", signal);
			
			return;
		}
```
3. cek apakah user found
```c
if(!found){
			signal = 1;
			sprintf(response, "%d User not found\n", signal);
			
			return;
		}
```
4. cek apakah root yang mengakses CREATE USER
```c
// cek apakah root
			if(strcmp(userQuery, "root")!=0){
				signal = 1;
				response[0] = '\0';
				sprintf(response, "%d Only root can CREATE USER\n", signal);
				return;
			}
```
5. cek apakah user already exist saat CREATE USER
```c
// cek apakah user already exist
			for(int i = 0; i<userCount;i++){
				if(strcmp(userName, user[i]) == 0){
					signal = 1;
					sprintf(response, "%d Username already exist\n", signal);
					
					return;
				}
			}
```
6. cek apakah database exist pada USE database
```c
// Error Case database not found
		if (file == NULL) {
			sprintf(response, "1 database %s not found.\n", databaseNameUse);
			return;
		}
```
7. cek apakah user ada di access.txt pada USE database
```c
// Check Apakah user memiliki akses atau user adalah root
		char names[100];
		while (fgets(names, sizeof(names), file) != NULL) {
			// Remove the newline character
			names[strcspn(names, "\n")] = '\0';

			if (strcmp(names, userQuery) == 0 || strcmp(userQuery, "root")==0) {
			    sprintf(response, "0 %s", databaseNameUse);
			    
			    logCommand(userQuery, databaseUsed, command);
			    fclose(file);
			    return;
			}
		}

		// Close the file
		fclose(file);
		
		// Case jika tidak memiliki akses
		sprintf(response, "1 You are not permitted to use this database.\n");
```
8. cek apakah root pada GRANT PERMISSION
```c
if(strcmp(userQuery, "root")!=0){
				signal = 1;
				response[0] = '\0';
				sprintf(response, "%d Only root can GRANT PERMISSION\n", signal);
				return;
			}
```
9. cek apakah database exist pada GRANT PERMIISON
```c
// Cek apakah database exist
			char databasePath[1000];
			sprintf(databasePath, "./databases/%s", databaseName);
			DIR *dir = opendir(databasePath);
    
			if (dir) {
				// printf("Directory exists.\n");
				closedir(dir);
			} else {
				sprintf(response, "1 database does not exist.\n");
				
				return;
			}
```
10. cek apakah user exist pada grant permission
```c
// Cek apakah user exist
			int found =0;
			for(int i = 0; i<userCount+2;i++){
				if(strcmp(userAccess, user[i])==0){
					found = 1;
					break;
				}
			}
			
			// jika user tidak ada
			if(!found){
				signal = 1;
				sprintf(response, "%d User not found\n", signal);
				
				return;
			}
```
11. cek apakah user sudah memiliki akses saat USE database
```c
// Check apakah user sudah memiliki akses
			char names[100];
			while (fgets(names, sizeof(names), file) != NULL) {
				// Remove the newline character
				names[strcspn(names, "\n")] = '\0';
				
				if (strcmp(names, userAccess) == 0) {
				    sprintf(response, "1 user already has access\n");
				    fclose(file);
				    return;
				}
			}
```
12. cek apakah databse sudah ada pada create database
```c
// cek apakah database sudah ada
			if (stat(databaseName, &st) == 0 && S_ISDIR(st.st_mode)) {
				response[0] = '\0';
				sprintf(response, "1 Database already exists.\n");
				
				return;
			} 
```
13. cek apakah sudah memilih database pada create table
```c
// cek apakah sudah memilih database
			if(strcmp(databaseUsed, "NoDatabase")==0){
				sprintf(response, "1 please select the database first\n");
				
				return;
			}
```
14. cek apakah table sudah exist saat create table
```c
// Check if the file exists
			if (access(filePath, F_OK) != -1) {
				sprintf(response, "1 table already exists: %s\n", tableName);
				return ;
			}
```
15. cek apakah table sudah ada pada drop table
```c
// cek if table file exist
			if (access(tablePath	, F_OK) == 0) {
				// printf("File exists.\n");
			} else {
				sprintf(response, "1 table does not exist.\n");
				return;
			}
```
16. cek apakah databse sudah ada pada drop database
```c
DIR *dir = opendir(databasePath);
    		
    		// cek if database exist
			if (dir == NULL) {
				sprintf(response, "1 Database not found.\n");
				return;
			}
```
17. Error syntax
```c
token = strtok(NULL, " ");
			if(strcmp(token, "INTO") != 0){
				sprintf(response, "1 error syntax\n");
				return;
			} 
```
```c
if(strcmp(token, "IDENTIFIED") != 0){
				sprintf(response, "1 Error syntax\n");
				
				return;
			} 
			
			token = strtok(NULL, " ");
			if(strcmp(token, "BY") != 0){
				sprintf(response, "1 Error syntax\n");
				
				return;
			} 
```
```c			
sprintf(response, "1 Error syntax\n");
	return;
```


# POIN I

Containerization
-Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan. Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.
-Publish Docker Image sistem ke Docker Hub. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.
-Untuk memastikan sistem kalian mampu menangani peningkatan penggunaan, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Sukolilo, Keputih, Gebang, Mulyos, dan Semolowaru dan jalankan Docker Compose di sana.


```
# Base image
FROM ubuntu:latest


# Install dependencies
RUN apt-get update && apt-get install -y gcc make

# Set working directory
WORKDIR /app


# Copy source code
COPY database/ /app/database/
COPY client/ /app/client/
COPY dump/ /app/dump/

# Compile database program
RUN gcc -o /app/database/database /app/database/database.c


# Compile client program
RUN gcc -o /app/client/client /app/client/client.c


# Compile dump program
RUN gcc -o /app/dump/client_dump /app/dump/client_dump.c


# Set entrypoint command
CMD ["/bin/bash"]

```
Setelah menyiapkan Dockerfile yang berisi langkah-langkah untuk mengatur lingkungan dan menentukan cara menjalankan aplikasi, berikut adalah perintah untuk membuild image menggunakan Docker CLI:

```
docker build -t storage-app .
```

![build_i](./img/build_i.png)


Lalu, setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya :

```
docker run storage-app
```


Untuk mempublikasikan Docker Image ke Docker Hub, perlu dilakukan langkah-langkah berikut:


Buat akun Docker Hub: Jika belum memiliki akun, buatlah akun Docker Hub di https://hub.docker.com/.


Pastikan memiliki Docker CLI terinstal di mesin dan telah masuk ke akun Docker Hub menggunakan perintah docker login. Ini akan mengotentikasi Docker CLI dengan akun Docker Hub.


Ubah nama Docker Image yang telah dibangun sebelumnya untuk mencerminkan nama pengguna Docker Hub. 

```
docker tag storage-app awan1016/storage-app
```

[ada perintah di atas tersebut akan membuat tag baru pada Docker Image yang Anda buat sebelumnya dengan nama yang sesuai dengan format nama_pengguna_docker_hub/nama_image.

kemudian lakukan unggah Docker Image yang telah ditandai ke Docker Hub menggunakan perintah berikut:

```
docker push awan1016/storage-app
```

Pada Perintah di atas tersebut akan melakukan pengunggahan Docker Image ke Docker Hub dengan nama "awan1016/storage-app". 
Kemudian setelah selesai melakukan unggah Docker Image, Docker Image dapat dilihat secara publik di profil Docker Hub. Docker Image Anda akan dapat dilihat secara publik pada :

https://hub.docker.com/r/awan1016/storage-app

![Docker](./img/Docker.png)

Kemudian setelah Docker Image dipublikasikan ke Docker Hub, maka ketikan akan menjalankan program database.c dan client.c secara bersamaan melalui docker, yaitu sebagai berikut :
Jika ingin menjalankan ./database dalam kontainer Docker dan kemudian menjalankan ./client dalam terminal baru tanpa menghentikan ./database, Anda dapat menggunakan fitur docker exec untuk masuk ke kontainer yang sudah berjalan dan menjalankan perintah di dalamnya.

Jalankan kontainer dengan perintah berikut:

```
sudo docker run -it awan1016/storage-app
```

Maka akan masuk ke shell kontainer.

Di dalam shell kontainer, jalankan ./database:

![database](./img/database.jpeg)


## Docker Compose
Untuk memastikan sistem kalian mampu menangani peningkatan penggunaan, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Sukolilo, Keputih, Gebang, Mulyos, dan Semolowaru dan jalankan Docker Compose di sana.

![compose_docker_folder](./img/compose_docker_folder.jpeg)

## docker compose setiap folder 
```
version: '3'
services:
  storage-app-1:
    image: awan1016/storage-app
    ports:
      - 8080:8080


  storage-app-2:
    image: awan1016/storage-app
    ports:
      - 8080:8080


  storage-app-3:
    image: awan1016/storage-app
    ports:
      - 8080:8080


  storage-app-4:
    image: awan1016/storage-app
    ports:
      - 8080:8080


  storage-app-5:
    image: awan1016/storage-app
    ports:
      - 8080:8080

```

masuk ke masing-masing folder untuk menjalankan Docker Compose di sana dengan perintah:
```
sudo docker-compose up -d
```

maka untuk output dari perintah tersebut, sebagai contoh di folder gebang 

![compose_docker](./img/compose_docker.png)


# POIN J
