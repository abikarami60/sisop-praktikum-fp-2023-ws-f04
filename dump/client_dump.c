#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 8080
#define BUFFER_SIZE 10000

int isRoot = 0;
char databaseUsed[100] = "NoDatabase";

int main(int argc, char* argv[]){
	for(int i=0;i<argc;i++){
		// printf("%d: %s\n", i, argv[i]);
	}
	
	uid_t uid = geteuid();
    
	if (uid == 0) {
	    // printf("Program executed by root.\n");
	    isRoot = 1;
	}
	
	// cek jumlah argumen 
	
	if(argc != 6 && !(isRoot)){
		printf("Jumlah argumen tidak tepat (harus 6)\n");
		return 0;
	}
	
	
	
	// setting socket 
	int sock = 0, valread;
	struct sockaddr_in serv_addr;
	char *message = "Hello from client!";


	// Create socket file descriptor
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
	perror("Socket creation failed");
	exit(EXIT_FAILURE);
	}

	// Configure server address
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);

	// Convert IP address from text to binary form
	if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
	perror("Invalid address or address not supported");
	exit(EXIT_FAILURE);
	}

	// Connect to the server
	if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
	perror("Connection failed");
	exit(EXIT_FAILURE);
	}

	char *user;
	char *pass;
	
	// authentication
	if(!isRoot){
		char buffer[BUFFER_SIZE] = {0};
		user = argv[2];
		pass = argv[4];
		int signal;
		char authMessage[200];
		sprintf(authMessage, "%d %s %s VERIFY %s %s", isRoot, user, databaseUsed, user, pass);
		send(sock, authMessage, strlen(authMessage), 0);
		// printf("Message sent to server\n");
	
		// receice response
		valread = read(sock, buffer, BUFFER_SIZE);
		// printf("%s", buffer);

		char temp[1024];
		strcpy(temp, buffer);
		char *token = strtok(temp, " ");
		signal = atoi(token);
    	
		token = strtok(NULL, " ");
		if(signal != 0){
			char errorMsg[100] = {0};
			while(token != NULL){
					strcat(errorMsg, token);
					strcat(errorMsg, " ");
					token = strtok(NULL, " ");
				}
				fprintf(stderr, "Error verifying user.\n");
				return 0;
    		} else {
    			// printf("OK\n");
    		}
	} else {
		char *root = "root";
		user = root;
	}
	
	// Akses
	char buffer[BUFFER_SIZE] = {0};
	char database[100];
	if(!isRoot){
		strcpy(database, argv[5]);
	} else {
		strcpy(database, argv[1]);
	}
	
	
	char accessMessage[500];
	sprintf(accessMessage, "%d %s %s USE %s", isRoot, user, databaseUsed, database);
	
	send(sock, accessMessage, strlen(accessMessage), 0);
	
	valread = read(sock, buffer, BUFFER_SIZE);
	
	int signal = 0; 
	char temp[10000] = {0};
	strcpy(temp, buffer);
	
	char* firstSpace = strchr(temp, ' ');
	int firstWordLength = firstSpace - temp;
	
	char signalStr[firstWordLength + 1]; // +1 for the null terminator
	strncpy(signalStr, temp, firstWordLength);
	signalStr[firstWordLength] = '\0'; // Null terminate the string
	signal = atoi(signalStr);
    
	char responseMessage[strlen(firstSpace + 1) + 1]; // +1 for the null terminator
	strcpy(responseMessage, firstSpace + 1);
	
	
	if(signal != 0){
		fprintf(stderr, "Error trying database.\n");
		return 0;
	} else {
		// printf("Succes Message: %d %s\n", signal, responseMessage);
		strcpy(databaseUsed, responseMessage);
	};
	
	buffer[0] = '\0';
	temp[0] = '\0';
	
	
	// dump
	signal = 0;
	char dumpMessage[500];
	sprintf(dumpMessage, "%d %s %s DUMP", isRoot, user, databaseUsed);
	
	send(sock, dumpMessage, strlen(dumpMessage), 0);
	
	valread = read(sock, buffer, BUFFER_SIZE);
	
	temp[10000] = '\0';
	strcpy(temp, buffer);
	
	firstSpace = strchr(temp, ' ');
	firstWordLength = firstSpace - temp;
	
	signalStr[firstWordLength + 1]; // +1 for the null terminator
	strncpy(signalStr, temp, firstWordLength);
	signalStr[firstWordLength] = '\0'; // Null terminate the string
	signal = atoi(signalStr);
	
	char dump[strlen(firstSpace + 1) + 1]; // +1 for the null terminator
	strcpy(dump, firstSpace + 1);
	
	if(signal != 0){
		printf("%d: %s\n", signal, dump);
		return 1;
	} else {
		printf("%s\n", dump);
		
	};
	
	
	return 0;
}